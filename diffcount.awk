#!/usr/bin/awk -f
function isatty(idx) {
	return system("[ -t " idx " ]") ? 0 : 1;
}
function abs(v) {
	return v < 0 ? -v : v;
}
BEGIN {
	st = 0;
	remove_cnt = 0;
	add_cnt = 0;
	context_cnt = 0;
	files_cnt = 0;
	if (length(format) == 0) {
		format = "human";
	}
	if (length(color) == 0 || color == "auto") {
		color = isatty(1);
		color_stderr = isatty(2);
	} else {
		color_stderr = color;
	}
	if (length(debug) == 0) {
		debug = 0;
	}
	if (length(debug_internal) == 0) {
		debug_internal = 0;
	}
	if (length(debug_skip_context) == 0) {
		debug_skip_context = 0;
	}
	if (length(debug_skip_filenames) == 0) {
		debug_skip_filenames = 0;
	}
	if (length(debug_skip_extra_headers) == 0) {
		debug_skip_extra_headers = 0;
	}
	if (length(debug_skip_chunk_headers) == 0) {
		debug_skip_chunk_headers = 0;
	}
}
function print_summary(additions, deletions) {
	switch (format) {
	case "human":
		new_ratio = (abs(additions - deletions) / (additions + deletions));
		if (color) {
			printf "\033[1;32m%d\033[0m additions\n", additions
			printf "\033[1;31m%d\033[0m deletions\n", deletions
			if (files_cnt > 1) {
				print files_cnt " total files";
			}
			if (more) {
				printf "\033[1;33m%d\033[0m net additions\n", (additions - deletions)
				printf "%.2f%c churn\n", ((1 - new_ratio) * 100), "%"
			}
		} else {
			printf "%d additions\n%d deletions\n", additions, deletions
			if (files_cnt > 1) {
				print files_cnt " total files";
			}
			if (more) {
				print (additions - deletions) " net additions";
				printf "%.2f%c churn\n", ((1 - new_ratio) * 100), "%"
			}
		}
		break
	case "compact":
	default:
		if (color != 0) {
			printf "\033[1;32m+%d\033[0m,\033[0;31m-%d\033[0m\n", additions, deletions;
		} else {
			print "+" additions ",-" deletions;
		}
		break
	}
}
function should_color(tgt) {
	switch (tgt) {
	case "/dev/stdout":
		return color;
	case "/dev/stderr":
		return color_stderr;
	default:
		return color;
	}
}
function is_chunk_header(l) {
	return (l ~ /^@{2}.*@{2}/) ? 1 : 0;
}
function print_debug_line(before_state, after_state, before_add_cnt, before_remove_cnt, l, tgt) {
	a_cnt = add_cnt - before_add_cnt;
	r_cnt = remove_cnt - before_remove_cnt;
	if (!should_color(tgt)) {
		if (debug_internal) {
			printf "%d,%d,%d,%d:", before_state, after_state, a_cnt, r_cnt > tgt;
		}
		print l > tgt;
		return;
	}
	skip = 0;
	switch (after_state) {
	case 3:
		if (is_chunk_header(l)) {
			if (debug_skip_chunk_headers) {
				skip = 1;
			}
			line_color = "\033[0;36m";
		} else if (l ~ /^-/) {
			line_color = "\033[0;31m";
		} else if (l ~ /^\+/) {
			line_color = "\033[0;32m";
		} else {
			if (debug_skip_context != 0) {
				skip = 1;
			}
			line_color = "\033[2m";
		}
		break
	case 1:
	case 2:
		if (l ~ /^-{3}\s+/ || l ~ /^\+{3}\s+/ || l ~ /^\*{3}\s+/) {
			if (debug_skip_filenames) {
				skip = 1;
			}
			line_color = "";
		} else {
			if (debug_skip_extra_headers) {
				skip = 1;
			}
			line_color = "\033[2m";
		}
		break
	default:
		if (debug_skip_extra_headers) {
			skip = 1;
		}
		line_color = "\033[2m";
		break
	}
	if (!skip) {
		if (debug_internal) {
			printf "%d,%d,\033[0;32m%d\033[0m,\033[0;31m%d\033[0m:", before_state, after_state, a_cnt, r_cnt > tgt;
		}
		print line_color l "\033[0m" > tgt;
	}
}
function do_increment(ty) {
	switch (ty) {
	case "+":
		add_cnt++;
		break
	case "-":
		remove_cnt++;
		break
	default:
		context_cnt++;
		break
	}
}
{
	before_st = st;
	before_add_cnt = add_cnt;
	before_remove_cnt = remove_cnt;
	switch (st) {
	# Await filename info
	case 0:
	case 1:
		if (($1 == "---" || $1 == "+++") && NF > 1 && length($2) > 0) {
			st = st + 1;
		} else if ($1 == "***" && NF > 1 && length($2) > 0) {
			st = 2;
		} else if (is_chunk_header($0)) {
			st = 3;
		} else {
			st = 0;
		}
		break
	# Await start of hunk for file
	case 2:
		files_cnt++;
		if (is_chunk_header($0)) {
			st = 3;
		}
		break
	# Inside of hunk
	case 3:
		# Starting new hunk
		if (is_chunk_header($0)) {
			st = 3;
			break
		}
		# Trailing header added by git?
		if ($0 == "-- ") {
			st = 0;
			break
		}
		if (match($0, /^([\-\+ ])/, arr)) {
			do_increment(arr[1]);
		} else {
			# End of the hunk
			if ($1 ~ /^-{3}$/ || $1 == "+++") {
				st = 1;
			} else {
				st = 0;
			}
		}
		break
	default:
		st = 0;
	}
	if (debug || dump) {
		print_debug_line(before_st, st, before_add_cnt, before_remove_cnt, $0, (dump && !debug) ? "/dev/stdout" : "/dev/stderr");
	}
}
END {
	print_summary(add_cnt, remove_cnt);
}
