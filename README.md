# diffcount

Simple [`awk`](https://www.gnu.org/software/gawk/manual/gawk.html)-based tool for generating `diff` statistics.

# Example

```bash
# Get the additions/deletions from the last 4 commits
git diff HEAD~4..HEAD | diffcount
```

# Options

| Option | Description |
| `-v dump=1` | Dump the diff to stdout as well as printing the summary |
